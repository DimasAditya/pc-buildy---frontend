import styled from 'styled-components';

export const NavigationContainer = styled.div`
  width: 100%;
  padding: 1rem 1.5rem;
  display: flex;
  align-items: center;
  border-bottom: 1px solid grey;


  .logo {
    width: 5rem;
    height: 5rem;
    margin: 0 0.5rem 0 0;
    object-fit: contain;
  }

  .title {
    margin: 0;
    font-size: 3rem;
    font-weight: 700;
    color: white;
  }

  
`