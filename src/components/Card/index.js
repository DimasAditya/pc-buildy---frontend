import React from 'react';
import PropTypes from 'prop-types';
import { CategoryCardContainer } from './style';

class CategoryCard extends React.Component {
  render() {
  const { image, title, selected } = this.props;

    return (
      <CategoryCardContainer selected={selected}>
        <img className="image" src={image} />
        <span className="title">{title}</span>
      </CategoryCardContainer>
    );
  }
}

CategoryCard.propTypes = {
  image: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
};

export default CategoryCard;