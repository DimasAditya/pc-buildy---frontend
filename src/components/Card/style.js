import styled from 'styled-components';

export const CategoryCardContainer = styled.div`
  width: 8rem;
  height: 12rem;
  display: flex;
  flex-direction: column;

  border: 4px solid white;
  border-radius: 4px;
  box-shadow: rgba(0, 0, 0, 0.06) 0px 2px 4px 0px;
  ${props => props.selected && `
    border: 2px solid #FFD500;
  `}


  .title {
    margin: 0.4rem;
    font-size: 0.7rem;
    font-weight: 600;
    color: white;
  }

  .image {
    width: 100%;
    height: 10rem;
    border-radius: 10px 10px 0 0;
    object-fit: cover;
    border-bottom: 1px solid grey;
  }

  @media screen and (max-width: 768px) {
    width: 6rem;
    height: 8rem;
  }

  &:hover {
    opacity: 0.8;
  }
`