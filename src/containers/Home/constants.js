export const DEFAULT_ACTION = 'src/Home/DEFAULT_ACTION';
export const POST_TRAVEL_INFO = 'src/Home/POST_TRAVEL_INFO';
export const POST_TRAVEL_INFO_SUCCESS = 'src/Home/POST_TRAVEL_INFO_SUCCESS';
export const POST_TRAVEL_INFO_FAILED = 'src/Home/POST_TRAVEL_INFO_FAILED';

export const ORIGIN_PC = [
  {
    label: 'PC',
    value: 'PC'
  },
];