import styled from 'styled-components';

export const HomePageContainer = styled.div`
  @keyframes fadeOut {
    from {opacity: 1;}
    to {opacity: 0;}
  }

  @keyframes fadeIn {
    from {opacity: 0;}
    to {opacity: 1;}
  }

  width: 100%;
  padding: 2rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: black;
  

  .subtitle {
    font-size: 1.5rem;
    font-weight: 600;
    color: dark;
  }

  .content {
    width: 40rem;
    max-width: 100%;
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    align-items: center;
    
  }

  .innerContentContainer {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    animation-name: fadeIn;
    animation-duration: 1s;
    color: white;
  }

  .categoryContainer {
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    flex-wrap: wrap;
    align-items: center;
    color: white;
    background: black;
    
  }

  .resultsContainer {
    width: 100%;
    display: flex;
    flex-direction: column;
  }

  .loadingContainer {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .loading {
    width: 20rem;
    height: 20rem;
    object-fit: contain;
  }


  .backButtonContainer {
    width: 100%;
    display: flex;
    padding-left: 15.6rem;
  }

  .backButton {
    margin: 2rem 0;
    padding: 0.5rem 3rem;
    border-radius: 2rem;
    font-size: 1rem;
    font-weight: 700;
    background: black;
    color: ${props => props.theme.colors.white};
  }

  .nextButtonContainer {
    width: 100%;
    display: flex;
    align-items: center;
    padding-left: 15rem;
  }

  .priceInputField {
    width: 100%;
    padding: 0.5rem 1rem;
    border-radius: 8rem;
    border: 2px solid darkgrey;
    color: black;
    font-size: 1rem;
  }

  .citySelect {
    width: 100%;
  }

  .nextButton {
    margin: 2rem 0;
    padding: 0.5rem 3rem;
    border-radius: 4rem;
    font-size: 1rem;
    font-weight: 700;
    background: white ;
    color: black;

    &:hover {
      background: #FFD500;
    }
  }

  @media screen and (max-width: 768px) {
    padding: 0.5rem;
    .content {
      max-width: 100%;
      min-width: 100%;
    }

    .subtitle {
      font-size: 1rem;
    }
  }
`