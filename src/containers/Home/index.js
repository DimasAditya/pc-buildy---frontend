import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Select from 'react-select';
import { HomePageContainer } from './style';
import Navigation from 'components/Navbar';
import CategoryCard from 'components/Card';

import GamePC from 'assets/gamming.jpeg';
import Designs from 'assets/Design.jpeg';
import Typing from 'assets/type.jpeg';
import Casual from 'assets/casual.jpeg';
import Loading from 'assets/loading.gif';
import Empty from 'assets/empty.gif';

import { postTravel } from './actions';

import { ORIGIN_PC } from './constants';

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 0,
      pcInfo: {
        budget: '',
        category: '',
        origin: '',
      }
    }
  }

  onChangeInfo = (field, value) => {
    const currentState = this.state.pcInfo;
    currentState[field] = value;
    this.setState({ pcInfo: currentState });
  }

  onClickNext = () => {
    const { currentStep } = this.state;
    if (currentStep === 2) {
      this.props.postTravel(this.state.pcInfo)
    }
    this.setState({ currentStep: currentStep + 1 });
  }

  onClickBack = () => {
    const { currentStep } = this.state;
    this.setState({ currentStep: currentStep - 1 });
  }

  showNextButton = () => {
    const { currentStep, pcInfo } = this.state;

    return (currentStep === 0 && pcInfo.category) ||
      (currentStep === 1 && pcInfo.budget) ||
      (currentStep === 2 && pcInfo.origin);
  }

  showBackButton = () => {
    const { currentStep } = this.state;

    return currentStep > 0;
  }

  getCategoryCards = () => {
    const currentCategory = this.state.pcInfo.category;
    const categories = [
      {
        category: 'Gamming',
        image: GamePC,
        selected: currentCategory === 'Gamming',
        title: 'Gaming PC',
      },
      {
        category: 'Design',
        image: Designs,
        selected: currentCategory === 'Design',
        title: 'Design PC',
      },
      {
        category: 'Typing',
        image: Typing,
        selected: currentCategory === 'Typing',
        title: 'Typing PC',
      },
      {
        category: 'Casual',
        image: Casual,
        selected: currentCategory === 'Casual',
        title: 'Casual PC',
      }
    ]

    return (
      categories.map((item, index) => (
        <button key={index} onClick={() => this.onChangeInfo("category", item.category)}>
          <CategoryCard image={item.image} title={item.title} selected={item.selected} />
        </button>
      ))
    );
  }

  renderFirstStep = () => (
    <div className="innerContentContainer">
      <p className="subtitle">
        What kind of PC do you want to build?
      </p>
      <div className="categoryContainer">
        {this.getCategoryCards()}
      </div>
    </div>
  )

  renderSecondStep = () => (
    <div className="innerContentContainer">
      <p className="subtitle">
        So , How much budged do you have?
      </p>
      <input
        type="number"
        className="priceInputField"
        placeholder = "Enter money in IDR"
        onChange={(evt) => this.onChangeInfo("budget", evt.target.value)}
        value={this.state.pcInfo.budget}
      />
    </div>
  )

  renderThirdStep = () => (
    <div className="innerContentContainer">
      <p className="subtitle">
        Awesome! Which city are you travelling from?
      </p>
      <Select
        className="citySelect"
        value={this.state.pcInfo.origin}
        onChange={(option) => this.onChangeInfo("origin", option)}
        options={ORIGIN_PC}
      />
    </div>
  )

  renderResults = () => (
    <div className="innerContentContainer">
      <p className="subtitle">
        {
          this.props.isLoading ? "Please wait... building your PC..."
          : this.props.travelRoutes.length === 0 ? "Sorry no PC can be build, incrase your budged please?"
          : "This is the List of Component to build your PC"
        }
      </p>
      <div className="resultsContainer">

      </div>
    </div>
  )

  renderContent = () => {
    const { currentStep } = this.state;
    if (currentStep === 0) {
      return this.renderFirstStep();
    } else if (currentStep === 1) {
      return this.renderSecondStep();
    } else if (currentStep === 2) {
      return this.renderResults()
    } 

    return null;
  }

  render() {
    return (
      <React.Fragment>
        <Navigation />
        <HomePageContainer>
          <div className="content">
            
            {this.renderContent()}
            <div className="nextButtonContainer">
              {
                this.showNextButton() &&
                  <button onClick={this.onClickNext} className="nextButton"> Confirm </button>
              }
            </div>
            <div className="backButtonContainer">
              {
                this.showBackButton() &&
                  <button onClick={this.onClickBack} className="backButton">Back</button>
              }
            </div>
          </div>
        </HomePageContainer>
      </React.Fragment>
    );
  }
}

HomePage.propTypes = {
  isLoading: PropTypes.bool,
  postTravel: PropTypes.func,
  travelRoutes: PropTypes.shape()
}

function mapStateToProps(state) {
  return {
    isLoaded: state.homePageReducer.get("isLoaded"),
    isLoading: state.homePageReducer.get("isLoading"),
    travelRoutes: state.homePageReducer.get("travelRoutes")
  }
}

function mapDispatchToProps(dispatch) {
  return {
    postTravel: (data) => dispatch(postTravel(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);