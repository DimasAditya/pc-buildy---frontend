export const theme = {
  'colors': {

    'DARK':'#E9967A',
    'LIGHT' : '#FFA07A',
    'black': '#545454',
    'blue': '#6F8D9A',
    'darkGrey': '#BEBEBE',
    'grey': '#EEEEEE',
    'pink': '#DCA9A3',
    'white': '#FFFFFF',
    'red' : '#DACA912'
  }
};