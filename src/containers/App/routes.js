import NotFoundPage from 'containers/404';
import HomePage from 'containers/Home';

export const routes = [
  {
    'component': HomePage,
    'exact': true,
    'path': '/'
  },
  { component: NotFoundPage }
];
